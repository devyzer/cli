<?php

namespace App\Http;

use App\Terminal\Config\Config;
use Illuminate\Http\Client\PendingRequest;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\Http;

class Client
{
    protected $headers = [];

    /**
     *  Default uri to generate token;
     */
    protected $redirectToBrowserUrl;

    /**
     * @var string
     */
    protected $fetchAccessToken = 'token';

    /**
     * @var string
     */
    protected $fetchAuthUserUrl = 'user';

    /**
     * @var string
     */
    private $createSiloUrl = 'silo';


    /**
     * @var string
     */
    private $getSiloInfoUrl = 'silo/{silo_id}';

    /**
     * @var string
     */
    private $terminateSiloUrl = 'silo/{silo_id}/terminate';

    /**
     * @var string
     */
    private $updateSiloFilesUrl = 'silo/{silo_id}/update-files';

    /**
     * @var string
     */
    private $updateSiloEnvUrl = 'silo/{silo_id}/update-env';

    /**
     * @var PendingRequest
     */
    private $httpClient;

    public function __construct()
    {
        $this->buildHttpClient();
    }

    public function getClient(): PendingRequest
    {
        return $this->httpClient;
    }

    protected function buildHttpClient(): void
    {
        $this->httpClient = Http::baseUrl(sprintf('%s/api', config('devyzer.base_url')));
    }

    protected function withMainHeaders(): Client
    {
        $this->httpClient->withHeaders([
            'Content-Type' => 'application/json',
            'Accept' => 'application/json',
        ]);

        return $this;
    }

    public function fetchAccessToken(string $key): string
    {
        $response = $this->withMainHeaders()->getClient()->post($this->fetchAccessToken, ['key' => $key]);

        return $response->throw()->json()['access_token'];
    }

    public function getAuthenticatedUser(string $token): bool
    {
        $response = $this->withMainHeaders()->authenticateAs($token)->getClient()->get($this->fetchAuthUserUrl);

        return $response->throw()->successful();
    }

    /**
     * @throws RequestException
     */
    public function deployProject(string $file_path, Config $config, string $token): ?array
    {
        $client = $token != ''
            ? $this->authenticateAs($token)->getClient()
            : $this->getClient();

        $data = [
            'source_type' => 'file',
            'hashed_dir' => hash('sha256', $config->getWorkingDir()),
            'name' => $config->getName(),
            'silo_id' => $config->getSiloId(),
            'external_domain' => $config->getExternalDomain(),
            'container' => $config->getContainer(),
            'install_script' => $config->getInstallScript(),
            'database_dump_path' => $config->getDatabaseDumpPath(),
            'env_variables' => $config->getEnvVariables(),
            'force' => $config->isForce(),
        ];

        $response = $client->attach('project_files', fopen($file_path, 'r'))
            ->post($this->createSiloUrl, $data);

        return $response->throw()->json()['data'];
    }

    /**
     * @throws RequestException
     */
    public function updateProjectFiles(string $updated_files, string $deleted_files, Config $config, string $token): ?array
    {
        $client = $token != ''
            ? $this->authenticateAs($token)->getClient()
            : $this->getClient();

        $data = [
            'hashed_dir' => hash('sha256', $config->getWorkingDir()),
            'deleted_files' => $deleted_files,
        ];

        $response = $client->attach('updated_files', fopen($updated_files, 'r'))
            ->post(str_replace('{silo_id}', $config->getSiloId(), $this->updateSiloFilesUrl), $data);

        return $response->throw()->json()['data'];
    }

    /**
     * @throws RequestException
     */
    public function updateProjectEnv(Config $config, string $token): ?array
    {
        $client = $token != ''
            ? $this->authenticateAs($token)->getClient()
            : $this->getClient();

        $data = [
            'env_variables' => $config->getEnvVariables(),
        ];

        $response = $client->post(str_replace('{silo_id}', $config->getSiloId(), $this->updateSiloEnvUrl), $data);

        return $response->throw()->json()['data'];
    }


    /**
     * @throws RequestException
     */
    public function getSiloInfo(Config $config, string $token)
    {
        $client = $token != ''
            ? $this->authenticateAs($token)->getClient()
            : $this->getClient();

        $response = $client->get(str_replace('{silo_id}', $config->getSiloId(), $this->getSiloInfoUrl));

        return $response->throw()->json()['data'];
    }

    public function terminate(Config $config, string $token)
    {
        $client = $token != ''
            ? $this->authenticateAs($token)->getClient()
            : $this->getClient();

        $response = $client->post(str_replace('{silo_id}', $config->getSiloId(), $this->terminateSiloUrl));

        return $response->throw()->json()['data'];
    }

    private function authenticateAs(string $token): Client
    {
        $this->httpClient->withHeaders(['Authorization' => "Bearer $token"]);

        return $this;
    }


}
