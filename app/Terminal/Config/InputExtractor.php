<?php

declare(strict_types=1);

namespace App\Terminal\Config;

use Symfony\Component\Console\Input\InputInterface;

final class InputExtractor
{
    private $input;

    public function __construct(InputInterface $input)
    {
        $this->input = $input;
    }

    public function getStringArgument(string $key, string $default = null): ?string
    {
        try {
            $argument = $this->input->getArgument($key);

            return $this->stringValueOrDefault($argument, $default);
        } catch (\Exception $exception) {
            return $default;
//            ds
        }
    }

    public function getStringOption(string $key, string $default = null): ?string
    {
        try {
            $option = $this->input->getOption($key);

            return $this->stringValueOrDefault($option, $default);
        } catch (\Exception $exception) {
            return $default;
        }
    }

    private function stringValueOrDefault($value, string $default = null): ?string
    {
        if ($value === null) {
            return $default;
        }

        if (is_array($value) && isset($value[0])) {
            return (string) $value[0];
        }

        return (string) $value;
    }

    public function getArrayOption(string $key): array
    {
        try {
            $option = $this->input->getOption($key);

            if (is_string($option) && ! empty($option)) {
                return explode(',', $option);
            }

            if (! is_array($option)) {
                return [];
            }

            return empty($option) ? [] : $option;
        } catch (\Exception $exception) {
            return [];
        }
    }

    public function getFloatOption(string $key): float
    {
        return (float) $this->input->getOption($key);
    }

    public function getBooleanOption(string $key): bool
    {
        try {
            return (bool) $this->input->getOption($key);
        } catch (\Exception $exception) {
            return  false;
        }
    }
}
