<?php

declare(strict_types=1);

namespace App\Terminal\Config;

use Illuminate\Support\Facades\File;

final class Config
{
    private $name;

    private $siloId;

    private $databaseDumpPath;

    private $container;

    private $useLocalEnv;

    private $extraEnv;

    private $externalDomain;

    private $installScript;

    private $force;

    private $watchList;

    private $workingDir;

    private const ENV_FILE_NAME = '.env';

    public function __construct(?string $name, ?string $siloId, ?string $databaseDumpPath, ?string $container, ?bool $useLocalEnv, ?string $extraEnv, ?string $externalDomain, ?string $installScript, ?bool $force, ?string $workingDir, WatchList $watchList)
    {
        $this->name = $name;
        $this->siloId = $siloId;
        $this->databaseDumpPath = $databaseDumpPath;
        $this->container = $container;
        $this->useLocalEnv = $useLocalEnv;
        $this->extraEnv = $extraEnv;
        $this->externalDomain = $externalDomain;
        $this->installScript = $installScript;
        $this->force = $force;
        $this->workingDir = $workingDir;
        $this->watchList = $watchList;
    }

    public static function fromArray(array $values): self
    {
        return new self(
            $values['name'] ?? null,
            $values['silo-id'] ?? null,
            $values['database-dump-path'] ?? null,
            $values['container'] ?? null,
            $values['use-local-env'] ?? null,
            $values['extra-env'] ?? null,
            $values['external-domain'] ?? null,
            $values['install-script'] ?? null,
            $values['force'] ?? false,
            $values['working-dir'] ?? getcwd(),
            new WatchList(
                $values['paths'] ?? [],
                $values['extensions'] ?? [],
                $values['ignore'] ?? []
            )
        );
    }

    public function merge(self $another): self
    {
        return new self(
            $another->name ?: $this->name,
            $another->siloId ?: $this->siloId,
            $another->databaseDumpPath ?: $this->databaseDumpPath,
            $another->container ?: $this->container,
            $another->useLocalEnv ?: $this->useLocalEnv,
            $another->extraEnv ?: $this->extraEnv,
            $another->externalDomain ?: $this->externalDomain,
            $another->installScript ?: $this->installScript,
            $another->force ?: $this->force,
            $another->workingDir ?: $this->workingDir,
            $another->watchList->merge($this->watchList)
        );
    }

    /**
     * @return string|null
     */
    public function getName(): ?string
    {
        return $this->name;
    }

    /**
     * @return string|null
     */
    public function getSiloId(): ?string
    {
        return $this->siloId;
    }

    /**
     * @return string|null
     */
    public function getDatabaseDumpPath(): ?string
    {
        return $this->databaseDumpPath;
    }

    /**
     * @return string|null
     */
    public function getContainer(): ?string
    {
        return $this->container;
    }

    /**
     * @return bool|null
     */
    public function getUseLocalEnv(): ?bool
    {
        return $this->useLocalEnv;
    }

    /**
     * @return string|null
     */
    public function getExtraEnv(): ?string
    {
        return $this->extraEnv;
    }

    /**
     * @return string|null
     */
    public function getExternalDomain(): ?string
    {
        return $this->externalDomain;
    }

    /**
     * @return string|null
     */
    public function getInstallScript(): ?string
    {
        return $this->installScript;
    }

    /**
     * @return bool|null
     */
    public function isForce(): ?bool
    {
        return $this->force;
    }

    /**
     * @return WatchList
     */
    public function getWatchList(): WatchList
    {
        return $this->watchList;
    }

    public function getWorkingDir(): string
    {
        return $this->workingDir;
    }

    public function getEnvVariables(): string
    {
        $env = '';
        if ($this->getUseLocalEnv()) {
            $env = File::get(sprintf('%s/%s', $this->getWorkingDir(), self::ENV_FILE_NAME));
        }

        if ($this->getExtraEnv()) {
            $env = sprintf("%s\n%s", $env, $this->getExtraEnv());
        }

        return $env;
    }
}
