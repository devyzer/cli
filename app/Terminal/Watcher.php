<?php

declare(strict_types=1);

namespace App\Terminal;

use App\Terminal\Config\WatchList;
use App\Terminal\Filesystem\ResourceWatcherBased\ChangesListener;
use React\EventLoop\LoopInterface;

final class Watcher
{
    private $loop;

    private $filesystemListener;

    public function __construct(LoopInterface $loop, ChangesListener $filesystemListener)
    {
        $this->loop = $loop;
        $this->filesystemListener = $filesystemListener;
    }

    public function startWatching(
        WatchList $watchList,
        callable $callable
    ): void {
        $this->filesystemListener->start($watchList);
        $this->filesystemListener->onChange($callable);

        $this->loop->run();
    }
}
