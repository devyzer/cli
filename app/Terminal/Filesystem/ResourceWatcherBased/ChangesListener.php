<?php

declare(strict_types=1);

namespace App\Terminal\Filesystem\ResourceWatcherBased;

use App\Terminal\Config\WatchList;
use Evenement\EventEmitter;
use React\EventLoop\LoopInterface;

final class ChangesListener extends EventEmitter implements
    \App\Terminal\Filesystem\ChangesListener
{
    private const INTERVAL = 0.15;

    private $loop;

    public function __construct(LoopInterface $loop)
    {
        $this->loop = $loop;
    }

    public function start(WatchList $watchList): void
    {
        $watcher = ResourceWatcherBuilder::create($watchList);

        $this->loop->addPeriodicTimer(
            self::INTERVAL,
            function () use ($watcher): void {
                $changes = $watcher->findChanges();
                if ($changes->hasChanges()) {
                    $this->emit('change', [$changes]);
                }
            }
        );
    }

    public function onChange(callable $callback): void
    {
        $this->on('change', $callback);
    }
}
