<?php

declare(strict_types=1);

namespace App\Terminal\Filesystem;

use App\Terminal\Config\WatchList;

interface ChangesListener
{
    public function start(WatchList $watchList): void;

    public function onChange(callable $callback): void;
}
