<?php

namespace App\Contracts;

use App\Services\ZipService;
use App\Terminal\Config\Config;

interface DevyzerContract
{
    public function openSilo(string $silo_url);

    /**
     * @param string $filepath
     * @param Config $config
     * @param string $token
     * @return mixed
     */
    public function deploy(string $filepath, Config $config, string $token = '');

    public function updateFiles(string $updated_files, string  $deleted_files, Config $config, string $token = '');

    public function updateEnv(Config $config, string $token = '');

    public function getSiloInfo(Config $config, string $token='');

    public function terminate(Config $config, string $token = '');

}
