<?php

namespace App\Contracts;

use App\Services\ZipService;
use App\Terminal\Config\Config;

interface ZipContract
{
    /**
     * @return bool | string
     */
    public function compress(?array $files = null);

    public function setWorkingDir(?string $path): ZipService;

    public function cleanUp(): void;
}
