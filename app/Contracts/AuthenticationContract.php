<?php

namespace App\Contracts;

interface AuthenticationContract
{
    public function check(string $key = null): bool;

    public function retrieveApiKey(): string;

    public function retrieveAccessToken(): string;

    public function launchBrowser(): void;

    public function storeNewApiKey(string $token): void;

    public function logout(): bool;
}
