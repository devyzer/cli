<?php

namespace App\Exceptions;

use Exception;
use Throwable;

class HttpException extends Exception
{
    protected $errors = [];

    public function __construct($message = '', $errors = [], $code = 0, Throwable $previous = null)
    {
        parent::__construct($message, $code, $previous);
        $this->errors = $errors;
    }

    public function getDetailedErrors()
    {
        return $this->errors;
    }
}
