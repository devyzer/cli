<?php

namespace App\Commands\Status;

use App\Contracts\AuthenticationContract;
use App\Contracts\DevyzerContract;
use App\Exceptions\HttpException;
use App\Terminal\Config\Builder;
use App\Terminal\Config\InputExtractor;
use App\Traits\Multitask;
use LaravelZero\Framework\Commands\Command;

class StatusCommand extends Command
{
    use Multitask;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'status {--c|config= : Path to config file} {--w|working-dir= : root working directory}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Get your silo\'s status';

    protected const CONFIG_FILE_NAME = 'devyzer.yaml';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(AuthenticationContract $auth, DevyzerContract $devyzer)
    {
        $input = new InputExtractor($this->input);
        $config = Builder::create()->fromConfigFile($input->getStringOption('config'));


        $this->multiTask('Deploying your project to Devyzer', function () use ($devyzer, $auth, $config): void {
            $this->tasks('Checking for authenticated user', function () use ($auth) {
                try {
                    if (!$auth->check()) {
                        if ($this->confirm('Only authenticated users can deploy to Devyzer, do you want to log in now?', 'yes')) {
                            return ($this->call('login')) == Command::SUCCESS;
                        }

                        return false;
                    }

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    $this->table(['details'], $e->getDetailedErrors());

                    return false;
                }
            });

            $this->tasks('Getting info', function () use ($devyzer, $auth, $config) {
                try {
                    $data = $devyzer->getSiloInfo($config, $auth->retrieveAccessToken());
                    $headers = ['Name', 'Status', 'Expiry Date'];

                    $data = [
                        [
                            'name' => $data['name'],
                            'status' => $data['status'],
                            'expired_at' => $data['expired_at'],
                        ],
                    ];
                    $this->info('');
                    $this->table($headers, $data);

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    if (count($e->getDetailedErrors()) > 0) {
                        $this->table(['details'], $e->getDetailedErrors());
                    }
                }
                return false;
            });

        });
    }

}
