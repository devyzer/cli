<?php

namespace App\Commands\Init;

use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;
use Symfony\Component\Yaml\Yaml;

class InitCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'init {--no-wizard}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Creates a Devyzer\'s Silo config in the current work directory.';

    protected const CONFIG_FILE_NAME = 'devyzer.yaml';

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = [
            'name' => null,
            'silo-id' => null,
            'database-dump-path' => null,
            'container' => 'php-80',
            'use-local-env' => true,
            'extra-env' => <<<ENV
DEBUG=true

ENV,
            'external-domain' => null,
            'install-script' => <<<Commands
composer install
mkdir storage/framework/sessions # Create sessions directory if not exists
# Fix file permissions
chown -R \$USER:www-data .
find . -type f -exec chmod 664 {} \;
find . -type d -exec chmod 775 {} \;
chgrp -R www-data storage bootstrap/cache
chmod -R ug+rwx storage bootstrap/cache
php artisan storage:link
Commands,
        ];

        if (! $this->option('no-wizard')) {
            $name = $this->output->ask('What is your project\'s name?');
            $deploymentId = $this->output->ask('What is your unique deployment id?');
            $dbDumpPath = $this->output->ask('What is the path of the DB dump path under your project\'s directory (optional) ?');
            $useLocalEnv = $this->output->confirm('Do you want to use your local .env variables when deploy your project ?');
            $container = $this->output->choice('What is your project\'s name?', [
                'php-80' => 'Laravel | Php 8.0',
                'php-74' => 'Laravel | Php 7.4',
                'php-73' => 'Laravel | Php 7.3',
                'php-72' => 'Laravel | Php 7.2',
                'php-71' => 'Laravel | Php 7.1',
                'php-56' => 'Laravel | Php 5.6',
            ]);

            $config = array_merge($config, [
                'name' => $name,
                'silo-id' => $deploymentId,
                'database-dump-path' => $dbDumpPath,
                'use-local-env' => $useLocalEnv,
                'container' => $container,
            ]);
        }

        if (! File::exists($this->configFileLocation())) {
            File::put($this->configFileLocation(), json_encode((object) []));
        }

        File::put($this->configFileLocation(), Yaml::dump($config, 2, 4, Yaml::DUMP_MULTI_LINE_LITERAL_BLOCK));
    }

    private function configFileLocation(): string
    {
        return sprintf('%s/%s', getcwd(), self::CONFIG_FILE_NAME);
    }
}
