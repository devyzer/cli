<?php

namespace App\Commands\Watch;

use App\Contracts\AuthenticationContract;
use App\Contracts\DevyzerContract;
use App\Contracts\ZipContract;
use App\Exceptions\HttpException;
use App\Services\ValidationService;
use App\Terminal\Config\Builder;
use App\Terminal\Config\Config;
use App\Terminal\Config\InputExtractor;
use App\Terminal\Config\WatchList;
use App\Terminal\Filesystem\ResourceWatcherBased\ChangesListener;
use App\Terminal\Watcher;
use App\Traits\Multitask;
use LaravelZero\Framework\Commands\Command;
use PhpZip\Exception\ZipException;
use React\EventLoop\Loop;
use Yosymfony\ResourceWatcher\ResourceWatcherResult;

class WatchCommand extends Command
{
    use Multitask;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'watch
       {--config     : Path to config file}
       {--p|paths=*  : Paths to watch}
       {--e|ext=*    : Extensions to watch}
       {--i|ignore=* : Paths to ignore}
      ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Deploy your local project in the current working directory to Devyzer\'s Silo.';

    private const CONFIG_FILE_NAME = 'devyzer.yaml';
    private const STATUS_READY = "Ready";
    private $file_name;

    private $config;

    private $zip;

    private $auth;

    private $validate;

    private $devyzer;

    private ?string $deploymentDirectory;

    public function __construct(ZipContract $zip, AuthenticationContract $auth, ValidationService $validate,DevyzerContract $devyzer)
    {
        parent::__construct();
        $this->zip = $zip;
        $this->auth = $auth;
        $this->validate = $validate;
        $this->devyzer = $devyzer;
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $config = $this->buildConfig(new InputExtractor($this->input));
        $this->config = $config;
        $this->zip->setWorkingDir($config->getWorkingDir());

        $this->multiTask('Watch and deploy to your Silo', function () use ($config): void {
            $this->tasks('Checking for authenticated user', function () {
                try {
                    if (!$this->auth->check()) {
                        if ($this->confirm('Only authenticated users can deploy to Devyzer, do you want to log in now?', 'yes')) {
                            return ($this->call('login')) == Command::SUCCESS;
                        }

                        return false;
                    }

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    $this->table(['details'], $e->getDetailedErrors());

                    return false;
                }
            });
            $this->tasks('Validating your configurations', function () use ($config) {
                try {
                    $data = $this->devyzer->getSiloInfo($config, $this->auth->retrieveAccessToken());
                    if ($data['status'] != self::STATUS_READY ) {
                        $this->warn(sprintf("\nYour Silo <%s> is not ready! ", $data['name']));
                        return false;
                    }
                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    $this->table(['details'], $e->getDetailedErrors());

                    return false;
                }
            });
            $this->tasks('Watching your changes', function (){
                $loop = Loop::get();
                $filesystem = new ChangesListener($loop);
                $this->line('');
                $watchList = $this->config->getWatchList();
                $watching = $watchList->isWatchingForEverything() ? '*.*' : implode(', ', $watchList->paths());
                $this->comment('Watching: ' . $watching);

                if ($watchList->hasIgnoring()) {
                    $this->comment('Ignoring: ' . implode(', ', $watchList->ignore()));
                }

                $watcher = new Watcher($loop, $filesystem);
                $watcher->startWatching($this->config->getWatchList(), [$this, 'filesChanged']);

            });
        });
    }

    public function filesChanged(ResourceWatcherResult $changes): void
    {
        try {
            $updated_files = array_merge($changes->getUpdatedFiles(), $changes->getNewFiles());
            $this->file_name = $this->zip->compress($updated_files);

            $this->devyzer->updateFiles($this->file_name, join(',', $changes->getDeletedFiles()), $this->config);
            $this->info("Your Silo is being synced");

        } catch (ZipException $e) {
            $this->error('Directory could not be compressed.');
        } catch (HttpException $e) {
            $this->error($e->getMessage());
            if (count($e->getDetailedErrors()) > 0) {
                $this->table(['errors'], $e->getDetailedErrors());
            }
        }
        $this->zip->cleanUp();
    }

    private function buildConfig(InputExtractor $input): Config
    {
        $builder = new Builder();
        $fromFile = $builder->fromConfigFile($input->getStringOption('config'));
        $fromCommandLineArgs = $builder->fromCommandLineArgs($input);

        return $fromFile->merge($fromCommandLineArgs);
    }
}
