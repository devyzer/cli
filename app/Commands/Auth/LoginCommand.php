<?php

namespace App\Commands\Auth;

use App\Contracts\AuthenticationContract;
use App\Exceptions\HttpException;
use LaravelZero\Framework\Commands\Command;

class LoginCommand extends Command
{
    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'login  {--key=}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Authenticates a User';

    /**
     * Execute the console command.
     *
     * @param AuthenticationContract $auth
     * @return mixed
     */
    public function handle(AuthenticationContract  $auth)
    {
        $status = $this->task('Authenticating', function () use ($auth) {
            try {
                if (! $auth->check()) {
                    return $this->triggerNewLogin($auth);
                }

                $this->info('Already authenticated');
            } catch (HttpException $e) {
                $this->error($e->getMessage());

                return false;
            }
        });

        return $status ? Command::SUCCESS : Command::FAILURE;
    }

    protected function triggerNewLogin(AuthenticationContract  $auth): bool
    {
        if ($this->option('key') != null) {
            $key = $this->option('key');
        } else {
            $this->newLine();
            $this->info('You would be redirected to your browser to obtain your access token.');
            $auth->launchBrowser();
            $this->info("In case you were not redirected, visit {$auth->getApiKeyUrl()} to get your Api key.");
            $key = $this->ask('Enter the authentication token copied from the browser');
        }

        try {
            if ($auth->check($key)) {
                $auth->storeNewApiKey($key);
                $this->info('Authentication was successful.');
            } else {
                $this->error('Invalid API key.');
            }

            return true;
        } catch (HttpException $e) {
            $this->error($e->getMessage());

            return false;
        }
    }
}
