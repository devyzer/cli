<?php

namespace App\Commands\Deploy;

use App\Contracts\AuthenticationContract;
use App\Contracts\ZipContract;
use App\Exceptions\HttpException;
use App\Services\DevyzerService;
use App\Services\ValidationService;
use App\Terminal\Config\Builder;
use App\Terminal\Config\Config;
use App\Terminal\Config\InputExtractor;
use App\Traits\Multitask;
use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;
use PhpZip\Exception\ZipException;

class DeployCommand extends Command
{
    use Multitask;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'deploy
            {--c|config= : Path to config file}
            {--f|force : Force deploy}
            {--w|working-dir= : root working directory} ';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Deploy your local project in the current working directory to Devyzer\'s Silo.';

    private $file_name;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(ZipContract $zip, DevyzerService $devyzer, AuthenticationContract $auth, ValidationService $validate)
    {
        $config = $this->buildConfig(new InputExtractor($this->input));
        $zip->setWorkingDir($config->getWorkingDir());
        $this->displayDetails($config);
        $this->multiTask('Deploying your project to Devyzer', function () use ($auth, $zip, $devyzer, $validate, $config): void {
            $this->tasks('Checking for authenticated user', function () use ($auth) {
                try {
                    if (! $auth->check()) {
                        if ($this->confirm('Only authenticated users can deploy to Devyzer, do you want to log in now?', 'yes')) {
                            return ($this->call('login')) == Command::SUCCESS;
                        }

                        return false;
                    }

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    $this->table(['details'], $e->getDetailedErrors());

                    return false;
                }
            });

            $this->tasks('Running pre-compression validation', function () use ($validate, $config) {
                if (! $validate->validate(getcwd(), ['hasComposer', 'composerIsValid'])) {
                    $this->error(implode("\n", $validate->errors()));

                    return false;
                }

                return true;
            });

            $this->tasks('Compressing files', function () use ($zip): bool {
                try {
                    $this->file_name = $zip->compress();

                    return true;
                } catch (ZipException $e) {
                    $this->error('Directory could not be compressed.');

                    return false;
                }
            });

            $this->tasks('Running pre upload validation', function () use ($validate): bool {
                if (! $validate->validate(getcwd(), ["size:$this->file_name"])) {
                    $this->error(implode("\n", $validate->errors()));

                    return false;
                }

                return true;
            });

            $this->tasks('Uploading your project and create Silo', function () use ($zip, $devyzer, $auth, $config): bool {
                try {
                    $response = $devyzer->deploy($this->file_name, $config, $auth->retrieveAccessToken());
                    $devyzer->openSilo($response['url']);
                    $this->info(sprintf("\nYou can access your Silo using this link: %s", $response['url']));

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    if (count($e->getDetailedErrors()) > 0) {
                        $this->table(['details'], $e->getDetailedErrors());
                    }
                }

                $zip->cleanUp();
                return false;
            });

            $this->tasks('Cleaning up', function () use ($zip) {
                $zip->cleanUp();

                return true;
            });
        });
    }

    protected function displayDetails(Config $config): void
    {
        $this->line('Devyzer deploy...');
        $content = [
            ['Deployment directory', $config->getWorkingDir()],
            ['Number of files', File::countFiles($config->getWorkingDir(), config('devyzer.ignore_files'))],
        ];

        $this->table([], collect($content));
    }

    private function buildConfig(InputExtractor $input): Config
    {
        $builder = new Builder();
        $fromFile = $builder->fromConfigFile($input->getStringOption('config'));
        $fromCommandLineArgs = $builder->fromCommandLineArgs($input);

        return $fromFile->merge($fromCommandLineArgs);
    }
}
