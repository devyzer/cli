<?php

namespace App\Commands\Terminate;

use App\Contracts\AuthenticationContract;
use App\Contracts\ZipContract;
use App\Exceptions\HttpException;
use App\Services\DevyzerService;
use App\Services\ValidationService;
use App\Terminal\Config\Builder;
use App\Terminal\Config\Config;
use App\Terminal\Config\InputExtractor;
use App\Traits\Multitask;
use Illuminate\Support\Facades\File;
use LaravelZero\Framework\Commands\Command;
use PhpZip\Exception\ZipException;

class TerminateCommand extends Command
{
    use Multitask;

    /**
     * The signature of the command.
     *
     * @var string
     */
    protected $signature = 'terminate {--c|config= : Path to config file}';

    /**
     * The description of the command.
     *
     * @var string
     */
    protected $description = 'Terminate your Silo';

    private $file_name;

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle(DevyzerService $devyzer, AuthenticationContract $auth)
    {
        $input = new InputExtractor($this->input);
        $config = Builder::create()->fromConfigFile($input->getStringOption('config'));

        $this->multiTask('Down your Silo', function () use ($auth, $devyzer, $config): void {
            $this->tasks('Checking for authenticated user', function () use ($auth) {
                try {
                    if (! $auth->check()) {
                        if ($this->confirm('Only authenticated users can deploy to Devyzer, do you want to log in now?', 'yes')) {
                            return ($this->call('login')) == Command::SUCCESS;
                        }

                        return false;
                    }

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    $this->table(['details'], $e->getDetailedErrors());

                    return false;
                }
            });

            $this->tasks('Down your Silo', function () use ($devyzer, $config, $auth): bool {
                try {
                    $response = $devyzer->terminate($config,$auth->retrieveAccessToken());
                    $this->info("\n Your Silo will be down.");

                    return true;
                } catch (HttpException $e) {
                    $this->error($e->getMessage());
                    if (count($e->getDetailedErrors()) > 0) {
                        $this->table(['details'], $e->getDetailedErrors());
                    }
                }
                return false;
            });
        });
    }

}
