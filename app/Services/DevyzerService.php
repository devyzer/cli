<?php

namespace App\Services;

use App\Contracts\BrowserContract;
use App\Contracts\DevyzerContract;
use App\Contracts\ZipContract;
use App\Exceptions\HttpException;
use App\Http\Client;
use App\Terminal\Config\Config;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\File;
use PhpZip\Exception\ZipException;
use PhpZip\Util\Iterator\IgnoreFilesRecursiveFilterIterator;
use PhpZip\ZipFile;
use RecursiveDirectoryIterator;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\Gitignore;

class DevyzerService implements DevyzerContract
{
    private Client $client;

    public function __construct()
    {
        $this->client = new Client();
    }

    /**
     * @return array|mixed|null
     */
    public function deploy(string $filepath, Config $config, string $token = '')
    {
        try {
            if ($response = $this->client->deployProject($filepath, $config, $token)) {
                return $response;
            }

            throw new HttpException('Could not deploy project.');
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to devyzer.io');
        } catch (RequestException $e) {
            $response = $e->response->json();

            if ($response['data'] && array_key_exists('messages', $response['data']))
                throw new HttpException($response['message'], $response['data']['messages']);
            else
                throw new HttpException($response['message']);
        }
    }

    /**
     * @return array|mixed|null
     */
    public function updateFiles(string $updated_files, string $deleted_files, Config $config, string $token = '')
    {
        try {
            if ($response = $this->client->updateProjectFiles($updated_files, $deleted_files, $config, $token)) {
                return $response;
            }

            throw new HttpException('Could not update files.');
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to devyzer.io');
        } catch (RequestException $e) {
            $response = $e->response->json();

            if ($response['data'] && array_key_exists('messages', $response['data']))
                throw new HttpException($response['message'], $response['data']['messages']);
            else
                throw new HttpException($response['message']);
        }
    }

    /**
     * @return array|mixed|null
     */
    public function updateEnv(Config $config, string $token = '')
    {
        try {
            if ($response = $this->client->updateProjectEnv($config, $token)) {
                return $response;
            }

            throw new HttpException('Could not update files.');
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to devyzer.io');
        } catch (RequestException $e) {
            $response = $e->response->json();

            if ($response['data'] && array_key_exists('messages', $response['data']))
                throw new HttpException($response['message'], $response['data']['messages']);
            else
                throw new HttpException($response['message']);
        }
    }

    /**
     * @return array|mixed|null
     */
    public function terminate(Config $config, string $token = '')
    {
        try {
            if ($response = $this->client->terminate($config, $token)) {
                return $response;
            }

            throw new HttpException('Could not update files.');
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to devyzer.io');
        } catch (RequestException $e) {
            $response = $e->response->json();

            if ($response['data'] && array_key_exists('messages', $response['data']))
                throw new HttpException($response['message'], $response['data']['messages']);
            else
                throw new HttpException($response['message']);
        }
    }

    public function openSilo(string $silo_url): void
    {
        $browser = app(BrowserContract::class);

        $browser->open($silo_url);
    }

    public function getSiloInfo(Config $config, string $token = '')
    {
        try {
            if ($response = $this->client->getSiloInfo($config, $token)) {
                return $response;
            }

            throw new HttpException('Could not get Silo info.');
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to devyzer.io');
        } catch (RequestException $e) {
            $response = $e->response->json();

            if ($response['data'] && array_key_exists('messages', $response['data']))
                throw new HttpException($response['message'], $response['data']['messages']);
            else
                throw new HttpException($response['message']);
        }
    }
}
