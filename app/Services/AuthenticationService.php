<?php

namespace App\Services;

use App\Contracts\AuthenticationContract;
use App\Contracts\BrowserContract;
use App\Exceptions\HttpException;
use App\Http\Client;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Http\Client\ConnectionException;
use Illuminate\Http\Client\RequestException;
use Illuminate\Support\Facades\File;

/**
 * Class AuthenticationService
 */
class AuthenticationService implements AuthenticationContract
{
    protected Client $client;

    /**
     *  Default uri to generate token;
     */
    protected string $apiKeyUrl = '';

    /**
     * Default uri to validate token
     */
    protected string $validateTokenUrl = '';

    /**
     * Default token storage store
     */
    protected string $apiKeyStorage = '';

    protected string $access_token = '';

    public function __construct()
    {
        $this->setApiKeyUrl()
            ->setApiKeyStorage()
            ->setValidateTokenUrl();

        $this->client = new Client();
    }

    protected function setApiKeyStorage(): AuthenticationService
    {
        $this->apiKeyStorage = config('devyzer.token_storage');

        return $this;
    }

    protected function setApiKeyUrl(): AuthenticationService
    {
        $this->apiKeyUrl = sprintf('%s/settings/api', config('devyzer.base_url'));

        return $this;
    }

    public function getApiKeyUrl(): string
    {
        return $this->apiKeyUrl ?: '';
    }

    protected function setValidateTokenUrl(): AuthenticationService
    {
        $this->validateTokenUrl = sprintf('%s/api/user', config('devyzer.base_url'));

        return $this;
    }

    /**
     * open users browser to retrieve token
     */
    public function launchBrowser(): void
    {
        $browser = app()->make(BrowserContract::class);
        $browser->open($this->apiKeyUrl);
    }

    private function fetchAccessToken(string $key): string
    {
        try {
            return $this->client->fetchAccessToken($key);
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to Devyzer');
        } catch (RequestException $e) {
            $response = $e->response->json();
            if ($response['data'] && array_key_exists('messages', $response['data']))
                throw new HttpException($response['message'], $response['data']['messages']);
            else
                throw new HttpException($response['message']);
        }
    }

    public function storeNewApiKey(string $apiKey): void
    {
        if (!is_dir(dirname($this->apiKeyStorage))) {
            mkdir(dirname($this->apiKeyStorage));
        }

        File::put($this->apiKeyStorage, $apiKey);
    }

    public function check($key = null): bool
    {
        if ($key == null && !$this->apiKeyFileExist()) {
            return false;
        }

        if ($key == null) {
            $key = File::get($this->apiKeyStorage);
        }

        try {
            if (!$this->access_token) {
                $this->access_token = $this->fetchAccessToken($key);
            }

            return $this->tokenIsValid($this->access_token);
        } catch (HttpException $e) {
            if ($this->apiKeyFileExist()) {
                $this->deleteApiKeyFile();
            }

            throw $e;
        }
    }

    protected function apiKeyFileExist(): bool
    {
        return File::isFile($this->apiKeyStorage);
    }

    private function tokenIsValid(string $token): bool
    {
        try {
            return $this->client->getAuthenticatedUser($token);
        } catch (ConnectionException $e) {
            throw new HttpException('Could not connect to Devyzer');
        } catch (RequestException $e) {
            $response = $e->response->json();

            throw new HttpException($response['message']);
        }
    }

    public function retrieveApiKey(): string
    {
        try {
            return File::get($this->apiKeyStorage);
        } catch (FileNotFoundException $e) {
            return '';
        }
    }

    public function retrieveAccessToken(): string
    {
        return $this->access_token;
    }

    public function logout(): bool
    {
        return $this->deleteApiKeyFile();
    }

    protected function deleteApiKeyFile(): bool
    {
        return unlink($this->apiKeyStorage);
    }
}
