<?php

namespace App\Services;

use App\Contracts\ZipContract;
use App\Http\Client;
use Illuminate\Contracts\Filesystem\FileNotFoundException;
use Illuminate\Support\Facades\File;
use PhpZip\Exception\ZipException;
use PhpZip\Util\Iterator\IgnoreFilesRecursiveFilterIterator;
use PhpZip\ZipFile;
use RecursiveDirectoryIterator;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\Gitignore;

class ZipService implements ZipContract
{
    protected array $ignoreFiles = [];

    protected ZipFile $zipper;

    protected string $fileStoragePath;

    protected string $gitDir = '.git';

    protected string $path;

    public function __construct()
    {
        $this->zipper = new ZipFile();
        $this->client = new Client();
        $this->ignoreFiles = config('devyzer.ignore_files');
        $this->setFileStorage();
    }

    protected function setFileStorage(): void
    {
        $this->fileStoragePath = config('devyzer.files_storage');
    }

    /**
     * @return bool|string
     * @throws ZipException
     */
    public function compress(?array $files=null)
    {
        return $this->createZip($files);
    }

    public function setWorkingDir(?string $path = null): ZipService
    {
        $this->path = $path ?? getcwd();

        return $this;
    }

    /**
     * @return bool| string
     * @throws ZipException
     */
    protected function createZip(?array $files = null)
    {
        if (! $files) {
            $directoryIterator = new RecursiveDirectoryIterator($this->getZipPath());

            $ignoreIterator = new IgnoreFilesRecursiveFilterIterator(
                $directoryIterator,
                $this->getExcludedFiles()
            );

            $this->zipper
                ->addFilesFromIterator($ignoreIterator);
        } else {
            foreach ($files as $file) {
                $this->zipper->addFile($file, str_replace($this->path, '', $file));
            }
        }

        $compressed_file_name = sha1(microtime()) . '.zip';

        $full_file_path = $this->getStoragePath($compressed_file_name);

        $this->zipper->saveAsFile($this->getStoragePath($compressed_file_name));

        return $full_file_path;
    }

    public function cleanUp(): void
    {
        File::cleanDirectory(config('devyzer.files_storage'));
    }

    protected function getZipPath(): string
    {
        return $this->path;
    }

    protected function getStoragePath($path = ''): string
    {
        if (! is_dir($this->fileStoragePath)) {
            mkdir($this->fileStoragePath, 0777, true);
        }

        return implode(DIRECTORY_SEPARATOR, [$this->fileStoragePath,$path]);
    }

    protected function getGitIgnoreFiles(): array
    {
        $finder = new Finder();
        $gitIgnoreFiles = [];

        try {
            $getRegex = Gitignore::toRegex(File::get($this->path . DIRECTORY_SEPARATOR . '.gitignore'));

            $file_paths = $finder->in($this->path)->exclude($this->ignoreFiles)->ignoreVCS(true)->name($getRegex);

            foreach (iterator_to_array($file_paths, true) as $file_path) {
                $gitIgnoreFiles[] = $file_path->getRelativePathname();
            }

            return $gitIgnoreFiles;
        } catch (FileNotFoundException $e) {
            return [];
        }
    }

    protected function isGitRepo(): bool
    {
        return File::isDirectory($this->path . DIRECTORY_SEPARATOR . $this->gitDir);
    }

    protected function getExcludedFiles(): array
    {
        return $this->isGitRepo()
            ? array_merge($this->getGitIgnoreFiles(), $this->ignoreFiles)
            : $this->ignoreFiles;
    }
}
