FROM php:8.0-cli-alpine

RUN mkdir /root/.devyzer

COPY ./builds/devyzer /bin/devyzer

WORKDIR /app

ENTRYPOINT ["devyzer"]
