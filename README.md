<p align="center">
  <img src="https://devyzer-web.s3.eu-west-1.amazonaws.com/logos/logo-beta-500.png" />
</p>

# Devyzer CLI
[Devyzer](https://devyzer.io) deploys your php laravel projects instantly into Silos (an online sandbox that is accessible anywhere) with minimal efforts. 

![Devyzer CLI screenshot](docs/devyzer-cli-screenshot.png)


# What is [Devyzer](https://devyzer.io)?

[Devyzer](https://devyzer.io) is a developer-first cloud-native deployment tool.
It will help you deploy your projects on sandboxes for testing and demo purposes. 
Devyzer supports now **Laravel**, and more is on the way!

[Learn more about what Devyzer can do and sign up for a free account »](https://devyzer.io)

# What is Devyzer CLI?

Devyzer CLI brings the functionality of [Devyzer](https://devyzer.io) into your local development environment. 
It can be run locally or in your CI/CD pipeline to deploy or run your Laravel projects on the cloud instantly 
in addition to  watch and sync  your local changes with your Silo's version.

## Supported languages and tools

Devyzer supports Laravel framework with php versions (`8.0|7.4|7.3|7.2|7.1|5.7`).
more languages and tools will be supported soon

---

# Install Devyzer CLI

Devyzer CLI can be installed through multiple channels.

## Install with composer

```bash
composer global require devyzer/cli
```

### Standalone executables

Use [Gitlab Releases](https://gitlab.com/devyzer/cli/-/releases) to download a standalone executable of Devyzer CLI for your platform.

### Devyzer CLI in a Docker image

Devyzer CLI can also be run from a Docker image. Devyzer offers multiple Docker images under [devyzer/cli](https://hub.docker.com/r/devyzer/cli). 
This images wrap the Devyzer CLI.

To run devyzer via docker :
  ```bash
  docker run -it --rm
      -v "<PROJECT_DIRECTORY>:/app"
      -v "config:/root/.devyzer"
    devyzer/cli:latest <DEVYZER_COMMAND> 
  ```

if you want to use your current working directory then replace `<PROJECT_DIRECTORY>` with `${PWD}` for linux and `%cd%` for windows.


---

# Getting started with Devyzer CLI

Once you installed the Devyzer CLI, you can verify it's working by running:

```bash
devyzer
```

See the [full Snyk CLI help](./docs/cli-commands).

## Authenticating Devyzer CLI

Devyzer CLI depends on [Devyzer.io](https://devyzer.io) APIs. Connect your Devyzer CLI with [Devyzer.io](https://devyzer.io) by running:

```bash
devyzer login
```

### Add Devyzer to your CI/CD

You can authorize Devyzer CLI in your CI/CD programmatically:

```bash
devyzer login --key=<Your API KEY>
devyzer deploy
```

# Getting support

If you need support using Devyzer CLI, please [contact support](https://devyzer.io).

We do not actively monitor GitHub Issues so any issues there may go unnoticed.

# Contributing

If you are an external contributor, before working on any contributions, please first [contact support](https://devyzer.io) to discuss the issue or feature request with us.

# Security

For any security issues or concerns, please see [SECURITY.md](SECURITY.md) file in this repository.

# Notices

## Devyzer API usage policy

The use of Devyzer's API, whether through the use of the 'devyzer' npm package or otherwise, is subject to the [Terms & Conditions](https://devyzer.io/privacy-policy).

---

Made with 💜 by Devyzer
